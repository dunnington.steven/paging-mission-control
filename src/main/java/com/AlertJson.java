package com;

import java.time.Instant;
import java.util.List;

/**
 * Provides the structure for printing an alert in JSON format to the console
 */
public class AlertJson {
	private int satelliteId;
	private String severity;
	private String component;
	private Instant timestamp;
	
	public AlertJson(int satelliteId, String severity, String component, Instant timestamp)
	{
		this.satelliteId = satelliteId;
		this.severity = severity;
		this.component = component;
		this.timestamp = timestamp;
	}
	
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		str.append("\t{\n");
		str.append("\t\t\"satelliteId\": " + this.satelliteId + ",\n");
		str.append("\t\t\"severity\": \"" + this.severity + "\",\n");
		str.append("\t\t\"component\": \"" + this.component + "\",\n");
		str.append("\t\t\"timestamp\": \"" + this.timestamp + "\"\n");
		str.append("\t}");
		
		return str.toString();
	}
	
	/**
	 * Used for testing purposes, returns equals if all fields are identical 
	 * @param alert
	 * @return
	 */
	@Override
	public boolean equals(Object alertObj)
	{
		if (alertObj == null) {
            return false;
        }

        if (alertObj.getClass() != this.getClass()) {
            return false;
        }
        
		AlertJson alert = (AlertJson) alertObj;
		return (this.satelliteId == alert.getSatelliteId() && this.severity.equals(alert.getSeverity()) && 
				this.component.equals(alert.getComponent()) && this.timestamp.equals(alert.getTimestamp()));
	}
	
	/**
	 * Print alerts in JSON format
	 * @param jsonOutputs
	 */
	public static void printAlerts(List<AlertJson> jsonOutputs)
	{
		System.out.println("[");
		for(int i = 0; i < jsonOutputs.size(); i++)
		{
			System.out.print(jsonOutputs.get(i).toString());
			if(i != jsonOutputs.size() - 1)
				System.out.println(",");
		}
		System.out.println("\n]");
	}
	
	public int getSatelliteId() {
		return satelliteId;
	}
	public void setSatelliteId(int satelliteId) {
		this.satelliteId = satelliteId;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public Instant getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}
}

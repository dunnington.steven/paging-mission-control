package com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

/**
 * Reads a text file, and outputs alerts in JSON format. See README for more details.
 * @author Steven Dunnington
 *
 */
public class PagingMissionControl {

	public static void main(String[] args)
	{
		if(args.length != 1)
		{
			System.out.println("Please pass in a text file.");
			System.exit(0);
		}
		
		//get text file
		File inputTxt = new File(args[0]);

		//HashMap to store Satellite objects, using the satellite's id for the key
		HashMap<Integer, Satellite> satelliteMap = getSatellites(inputTxt);
		 
		List<AlertJson> jsonOutputs = new ArrayList<AlertJson>();
		for(Satellite currentSatellite : satelliteMap.values())
		{
			jsonOutputs.addAll(currentSatellite.getAlerts());
		}
		 
		//print as JSON object		
		AlertJson.printAlerts(jsonOutputs);
	}
	
	/**
	 * Read the text file and get all Satellite objects. Returns a HashMap where the key 
	 * is the satellie's Id, and the value is the corresponding Satellite object
	 * @param inputTxt
	 * @return
	 */
	private static HashMap<Integer, Satellite> getSatellites(File inputTxt)
	{
		try
		{
			FileReader fileReader = new FileReader(inputTxt);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			//HashMap to store Satellite objects, using the satellite's id for the key
			HashMap<Integer, Satellite> satelliteMap = new HashMap<Integer, Satellite>();
			
			//read the file line by line
			String lineText;
			while ((lineText = bufferedReader.readLine()) != null) {
				//line format: 20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
				 
				//create SatelliteReading object
				String[] data = lineText.split("\\|");
				SatelliteReading satelliteReading = new SatelliteReading(data);
				 
				//add or update satelliteMap
				Satellite satellite = satelliteMap.get(satelliteReading.getId());
				if(satellite == null)
				{
					satellite = new Satellite(satelliteReading.getId());
					satellite.addSatelliteReading(satelliteReading);
					satelliteMap.put(satellite.getId(), satellite);
				} else 
				{
					 //update value
					 satellite.addSatelliteReading(satelliteReading);
				} 
			}
			bufferedReader.close();
			fileReader.close();
			
			return satelliteMap;
		}
		catch(Exception e)
		{
			System.out.println(e.getStackTrace());
			System.out.println("Error reading file.");
			System.exit(0);
		}
		return null;
	}
	
	/**
	 * Takes the date as given in the input file, and returns as a Java.time.Instant object
	 * @param stringDate
	 * @return
	 */
	public static Instant stringToInstant(String stringDate)
	{
		//add commas between year-month-day, T before the time, and Z at the end
		StringBuilder builderDate = new StringBuilder();
		builderDate.append(stringDate);
		builderDate.insert(4, "-");
		builderDate.insert(7, "-");
		builderDate.replace(10, 11, "T");
		builderDate.append("Z");
		
		return Instant.parse(builderDate.toString());
	}
}

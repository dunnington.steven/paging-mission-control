package com;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Satellite {

	private int id;
	private List<SatelliteReading> satelliteReadings;
	
	public Satellite(int id)
	{
		this.id = id;
		satelliteReadings = new ArrayList<SatelliteReading>();
	}
	
	/**
	 * Return all alerts for the Satellite
	 * @return
	 */
	public List<AlertJson> getAlerts()
	{
		List<AlertJson> alerts = new ArrayList<AlertJson>();
		
		Instant firstDate = null;
		Instant secondDate = null;
		
		Instant firstBattDate = null;
		Instant secondBattDate = null;
		Instant firstTstatDate = null;
		Instant secondTstatDate = null;
		
		boolean volatageUnderLimit;
		boolean temperatureAboveLimit;
		
		String severity;
		
		for(SatelliteReading reading : this.satelliteReadings)
		{
			volatageUnderLimit = reading.getComponent().equals("BATT") && reading.getValue() < reading.getRedLowLimit();
			temperatureAboveLimit = reading.getComponent().equals("TSTAT") && reading.getValue() > reading.getRedHighLimit();		
			if(volatageUnderLimit || temperatureAboveLimit)
			{
				//grab either the TSTAT or BATT variables to check
				if(volatageUnderLimit)
				{
					firstDate = firstBattDate;
					secondDate = secondBattDate;
				}
				else
				{
					firstDate = firstTstatDate;
					secondDate = secondTstatDate;
				}
				
				if(firstDate == null)
					firstDate = reading.getDate();
				else if(reading.getDate().minusSeconds(300).isBefore(firstDate)) //within 5 seconds
				{
					//set secondThermostat if it is null
					if(secondDate == null)
						secondDate = reading.getDate();
					else
					{
						//add alert, reset firstDate to be secondDate, and add the current date to be the new secondDate
						severity = temperatureAboveLimit ? "RED HIGH" : "RED LOW";
						AlertJson alert = new AlertJson(this.id, severity, reading.getComponent(), firstDate);
						alerts.add(alert);
						
						firstDate = secondDate;
						secondDate = reading.getDate();
					}
				}
				else
				{
					//not  within 5 seconds of the first reading, check if within 5 seconds of second reading if it exists
					if(secondDate == null)
						firstDate = reading.getDate();
					else if(reading.getDate().minusSeconds(300).isBefore(secondDate)) //within 5 seconds
					{
						firstDate = secondDate;
						secondDate = reading.getDate();
					}
				}
				
				//update either the TSTAT or BATT variables
				if(volatageUnderLimit)
				{
					firstBattDate = firstDate;
					secondBattDate = secondDate;
				}
				else
				{
					firstTstatDate = firstDate;
					secondTstatDate = secondDate;
				}
			}
		}
		return alerts;
	}
	
	public void addSatelliteReading(SatelliteReading satelliteReading)
	{
		satelliteReadings.add(satelliteReading);
	}

	public List<SatelliteReading> getSatelliteReadings() {
		return satelliteReadings;
	}

	public void setSatelliteReadings(List<SatelliteReading> satelliteReadings) {
		this.satelliteReadings = satelliteReadings;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}

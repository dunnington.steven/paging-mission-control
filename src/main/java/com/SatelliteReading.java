package com;

import java.time.Instant;

public class SatelliteReading {

	private int id;
	private Instant date;
	private int redHighLimit;
	private int redLowLimit;
	private int yellowHighLimit;
	private int yellowLowLimit;
	private float value;
	private String component;

	public SatelliteReading(String[] data) {
		this.date = PagingMissionControl.stringToInstant(data[0]);
		this.id = Integer.parseInt(data[1]);
		this.redHighLimit = Integer.parseInt(data[2]);
		this.yellowHighLimit = Integer.parseInt(data[3]);
		this.yellowLowLimit = Integer.parseInt(data[4]);
		this.redLowLimit = Integer.parseInt(data[5]);
		this.value = Float.parseFloat(data[6]);
		this.component = data[7];
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRedHighLimit() {
		return redHighLimit;
	}

	public void setRedHighLimit(int redHighLimit) {
		this.redHighLimit = redHighLimit;
	}

	public int getRedLowLimit() {
		return redLowLimit;
	}

	public void setRedLowLimit(int redLowLimit) {
		this.redLowLimit = redLowLimit;
	}

	public int getYellowHighLimit() {
		return yellowHighLimit;
	}

	public void setYellowHighLimit(int yellowHighLimit) {
		this.yellowHighLimit = yellowHighLimit;
	}

	public int getYellowLowLimit() {
		return yellowLowLimit;
	}

	public void setYellowLowLimit(int yellowLowLimit) {
		this.yellowLowLimit = yellowLowLimit;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

}

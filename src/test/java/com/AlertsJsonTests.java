package com;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AlertsJsonTests {
	
	@Test
	@DisplayName("Test toString method")
	public void toStringTest()
	{
		Instant instant = Instant.parse("2018-01-01T23:01:38.001Z");
		AlertJson alertJson = new AlertJson(1000, "RED HIGH", "TSTAT", instant);
		String toStringOutput = "\t{\n"
				+ "\t\t\"satelliteId\": 1000,\n"
				+ "\t\t\"severity\": \"RED HIGH\",\n"
				+ "\t\t\"component\": \"TSTAT\",\n"
				+ "\t\t\"timestamp\": \"2018-01-01T23:01:38.001Z\"\n"
				+ "\t}";
		assertEquals(toStringOutput, alertJson.toString());
		
		instant = Instant.parse("2018-01-01T23:01:09.521Z");
		alertJson = new AlertJson(1000, "RED LOW", "BATT", instant);
		toStringOutput = "\t{\n"
				+ "\t\t\"satelliteId\": 1000,\n"
				+ "\t\t\"severity\": \"RED LOW\",\n"
				+ "\t\t\"component\": \"BATT\",\n"
				+ "\t\t\"timestamp\": \"2018-01-01T23:01:09.521Z\"\n"
				+ "\t}";
		assertEquals(toStringOutput, alertJson.toString());

	}
}

package com;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.time.Instant;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class PagingMissionControlTests {

	@Test
	@DisplayName("Test stringToInstant method")
	public void stringToInstantTest()
	{
		String dateString = "20180101 23:01:38.001";
		Instant instant = Instant.parse("2018-01-01T23:01:38.001Z");
		assertEquals(instant, PagingMissionControl.stringToInstant(dateString));
		dateString = "20180101 23:01:09.521";
		instant = Instant.parse("2018-01-01T23:01:09.521Z");
		assertEquals(instant, PagingMissionControl.stringToInstant(dateString));
		dateString = "20221212 07:01:09.001";
		instant = Instant.parse("2022-12-12T07:01:09.001Z");
		assertEquals(instant, PagingMissionControl.stringToInstant(dateString));
	}
	
}

package com;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SatelliteTests {

	@Test
	@DisplayName("Test getAlerts method with TSTAT")
	public void getAlertsTestTstat()
	{
		//setup objects
		Satellite satellite = new Satellite(1001);
		String[] data = {"20180101 23:01:38.001", "1001", "101", "98", "25", "20", "102.9", "TSTAT"};
		SatelliteReading reading = new SatelliteReading(data);
		satellite.addSatelliteReading(reading);
		
		data = new String[] {"20180101 23:03:03.008", "1001", "101", "98", "25", "20", "102.7", "TSTAT"};
		reading = new SatelliteReading(data);
		satellite.addSatelliteReading(reading);
		
		data = new String[] {"20180101 23:03:05.009", "1001", "101", "98", "25", "20", "101.2", "TSTAT"};
		reading = new SatelliteReading(data);
		satellite.addSatelliteReading(reading);

		List<AlertJson> jsonAlerts = new ArrayList<AlertJson>();
		Instant date = Instant.parse("2018-01-01T23:01:38.001Z");
		AlertJson alert = new AlertJson(1001, "RED HIGH", "TSTAT", date);
		jsonAlerts.add(alert);
		
		assertEquals(jsonAlerts, satellite.getAlerts());

		//check 2 alerts back to back
		data = new String[] {"20180101 23:04:05.009", "1001", "101", "98", "25", "20", "101.2", "TSTAT"};
		reading = new SatelliteReading(data);
		satellite.addSatelliteReading(reading);
		
		date = Instant.parse("2018-01-01T23:03:03.008Z");
		alert = new AlertJson(1001, "RED HIGH", "TSTAT", date);
		jsonAlerts.add(alert);
		
		assertEquals(jsonAlerts, satellite.getAlerts());
	}
	
	@Test
	@DisplayName("Test getAlerts method with BATT")
	public void getAlertsTestBatt()
	{
		//setup objects
		Satellite satellite = new Satellite(1000);
		String[] data = {"20180101 23:01:09.521", "1000", "17", "15", "9", "8", "7.8", "BATT"};
		SatelliteReading reading = new SatelliteReading(data);
		satellite.addSatelliteReading(reading);
		
		data = new String[] {"20180101 23:02:11.302", "1000", "17", "15", "9", "8", "7.7", "BATT"};
		reading = new SatelliteReading(data);
		satellite.addSatelliteReading(reading);
		
		data = new String[] {"20180101 23:04:11.531", "1000", "17", "15", "9", "8", "7.9", "BATT"};
		reading = new SatelliteReading(data);
		satellite.addSatelliteReading(reading);

		List<AlertJson> jsonAlerts = new ArrayList<AlertJson>();
		Instant date = Instant.parse("2018-01-01T23:01:09.521Z");
		AlertJson alert = new AlertJson(1000, "RED LOW", "BATT", date);
		jsonAlerts.add(alert);
		
		assertEquals(jsonAlerts, satellite.getAlerts());

	}
}
